$(document).ready(function() {

  //Media Queries-----------------------------------------------
	var queries = [
	  {
	    context: 'range_0',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
	      mobileTeamColumns();
	      mobileProfileColumns();
	      mobileSidebar();
        document.documentElement.id = 'range_0';
        console.log('current range:', MQ.new_context);
	    },
	    unmatch: function() {
	    	slideDrawerDisable();
	    	unalternateSections();
	    	mobileTeamColumns();
	    	mobileSidebar();
	    	mobileProfileColumns();
	    }
	  },
	  {
	    context: 'range_1',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
	      unalternateSections();
	      mobileTeamColumns();
	      mobileProfileColumns();
	      mobileSidebar();
        document.documentElement.id = 'range_1';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	      unalternateSections();
	      mobileTeamColumns();
	      mobileProfileColumns();
	      mobileSidebar();
	      //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_2',
	    match: function() {
	      //responsiveEqualHeight(); 
	      slideDrawerEnable();
	      unalternateSections();
	      desktopTeamColumns();
	      desktopProfileColumns();
	      mobileSidebar();
        document.documentElement.id = 'range_2';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	      unalternateSections();
	      mobileSidebar();
	      //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_3',
	    match: function() {
		    alternateSections();
		    desktopTeamColumns();
		    desktopProfileColumns();
	      slideDrawerDisable();
	      desktopSidebar();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_3';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
  	    //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_4',
	    match: function() {
		    alternateSections();
		    desktopTeamColumns();
		    desktopProfileColumns();
	      slideDrawerDisable();
	      desktopSidebar();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_4';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
  	    //responsiveEqualHeightReset();
	    }
	  },
	  {
	    context: 'range_5',
	    match: function() {
		    alternateSections();
	      slideDrawerDisable();
	      desktopTeamColumns();
	      desktopSidebar();
	      desktopProfileColumns();
	      //responsiveEqualHeight();
        document.documentElement.id = 'range_5';
        console.log('current range:', MQ.new_context);
	    }
	  }
	];
	MQ.init(queries);
	var range = MQ.new_context;
	
	
/*
	if (localStorage.getItem("spanish") === null) {
		//show English
		$('span.en').show();
		$('span.sp').hide();
  }else{ 
	  //show Spanish
		$('span.sp').show();
		$('span.en').hide();
	}
*/

	

	$('header .toggle-language').click(function(e){
		e.preventDefault();
		if (localStorage.getItem("spanish") === null) {
			localStorage.setItem("spanish", JSON.stringify("woo"));
			/*
$('span.en').hide();
			$('span.sp').show();
*/
			$('header .toggle-language').text("View in English");
			$('span.sp').each(function(){
				var sptext = $(this).text();
				if(sptext == ''){
					$(this).hide();
					$(this).prev('span.en').show();
				}else{
					$(this).show();
					$(this).prev('span.en').hide().removeClass('override');
				}
			});
			if ($('body.parent-2').length){
				categorySpanish();
			}	
	  }else{
			localStorage.removeItem("spanish");
			$('span.en').show(); 
			$('span.sp').hide();
			$('header .toggle-language').text("Ver en Español");
			console.log("There is Spanish loaded, show English");
			if ($('body.parent-2').length){
				categoryEnglish();
			}	
		}
	});
	if(localStorage.getItem("spanish")){
		localStorage.setItem("spanish", JSON.stringify("woo"));

		$('header .toggle-language').text("View in English");
		$('span.sp').each(function(){
				var sptext2 = $(this).text();
				if(sptext2 == ''){
					$(this).hide();
					$(this).prev('span.en').show().addClass('override');
				}else{
					$(this).show();
					$(this).prev('span.en').hide().removeClass('override');;
				}
			});
		console.log("There was Spanish loaded, show Spanish");
		if ($('body.parent-2').length){
			categorySpanish();
		}	
	}else{
		localStorage.removeItem("spanish");
		$('span.en').show();
		$('span.sp').hide();
		$('header .toggle-language').text("Ver en Español");
		console.log("There was no Spanish loaded, show English");
		if ($('body.parent-2').length){
			categoryEnglish();
		}	
	}
	
	function categorySpanish(){
		$('#sidebar ul.team-navigation span').each(function(){
			var category = $(this).text();
			if(category == "Leadership"){
				$(this).text("Liderazgo");
			}else if(category == "Board of Directors"){
				$(this).text("Mesa Directiva");
			}else if(category == "Real Estate and Community Development"){
				$(this).text("Desarrollo de Comunidades");
			}else if(category == "Property Management Services"){
				$(this).text("Administración y Mantenimiento de Bienes Raíces");
			}else if(category == "Asset Management Services"){
				$(this).text("Administración de Activos");
			}else if(category == "New Markets Tax Credit Services"){
				$(this).text("Créditos New Markets Tax Credits");
			}else if(category == "Sustainability Services"){
				$(this).text("Sostenibilidad");
			}else if(category == "Corporate Operations"){
				$(this).text("Operaciones Corporativas");
			}
		});
	}
	
	function categoryEnglish(){
		$('#sidebar ul.team-navigation span').each(function(){
			var category = $(this).text();
			if(category == "Liderazgo"){
				$(this).text("Leadership");
			}else if(category == "Mesa Directiva"){
				$(this).text("Board of Directors");
			}else if(category == "Desarrollo de Comunidades"){
				$(this).text("Real Estate and Community Development");
			}else if(category == "Administración y Mantenimiento de Bienes Raíces"){
				$(this).text("Property Management Services");
			}else if(category == "Administración de Activos"){
				$(this).text("Asset Management Services");
			}else if(category == "Créditos New Markets Tax Credits"){
				$(this).text("New Markets Tax Credit Services");
			}else if(category == "Sostenibilidad"){
				$(this).text("Sustainability Services");
			}else if(category == "Operaciones Corporativas"){
				$(this).text("Corporate Operations");
			}
		});
	}
	
	
	
	$('.snap-drawers .toggle-language').click(function(e){
		e.preventDefault();
		if (localStorage.getItem("spanish") === null) {
			localStorage.setItem("spanish", JSON.stringify("woo"));
			$('span.en').hide();
			$('span.sp').show();
			$('.snap-drawers .toggle-language').text("View in English");
			if ($('body.parent-2').length){
				categorySpanish();
			}	
	  }else{
			localStorage.removeItem("spanish");
			$('span.en').show(); 
			$('span.sp').hide();
			$('.snap-drawers .toggle-language').text("Ver en Español");
			if ($('body.parent-2').length){
				categoryEnglish();
			}	
		}
	});
	if(localStorage.getItem("spanish")){
		$('span.en').hide();
		$('span.sp').show();
		$('.snap-drawers .toggle-language').text("View in English"); 
		if ($('body.parent-2').length){
			categorySpanish();
		}	
	}
	
	
	
	
/*
	if (localStorage.getItem("save") === null) {
		 document.getElementById('modal').style.display = 'block';
	   location.hash = 'modal';
	   localStorage.setItem("save", JSON.stringify("woo"));
  }else{
	   document.getElementById('modal').style.display = 'none';
	}
*/

	 
	
	if($('body.type-3').length || $('body.type-11').length){
		if($('body.page-1073').length){
			$('#sidebar.navigation ul li a[title="Key Team Members"]').parent().addClass('active');
		}
		$('#team-search').insertAfter('#sidebar.navigation ul li.last.active'); 
		//$('.team-navigation').insertAfter('.en .project-toolbar');
		//$('.team-navigation').insertAfter('.sp .project-toolbar');
	}
	

	

	
	if($('body.type-4').length){
		$('#what-we-do table').each(function(){
			var $children = $(this).find('td');
	    for(var i = 0, l = $children.length; i < l; i += 2) {
	      $children.slice(i, i+2).wrapAll('<tr></tr>');
	    }
		});
    $('#what-we-do table').fadeTo( "fast" , 1);    
	}
	
	
	if($('body.type-5').length || $('body.type-10').length){
		/*
var $children = $('#project-profiles td');
    for(var i = 0, l = $children.length; i < l; i += 3) {
      $children.slice(i, i+3).wrapAll('<tr></tr>');
    }
*/
    
    $('#project-profiles table').fadeTo( "fast" , 1);   
    
    $('#project-profiles tr tr').unwrap();
    
    function getUrlParameter(sParam)
		{
		    var sPageURL = window.location.search.substring(1);
		    var sURLVariables = sPageURL.split('&');
		    for (var i = 0; i < sURLVariables.length; i++) 
		    {
		        var sParameterName = sURLVariables[i].split('=');
		        if (sParameterName[0] == sParam) 
		        {
		            return sParameterName[1];
		        }
		    }
		} 
		var category = getUrlParameter('tag');  
		if(category != '' && category != null && category != 'undefined'){
			var categoryHeader = category.replace(/\+/g, ' ');
		}
		$('#sidebar.categories a').each(function(){
			var title = $(this).attr("title");
			if(category == title){
				$(this).parent().addClass('active'); 
			}
		});
		
		$('nav ul ul').hide();
    
	} //if 
	
	if($('body.type-8').length){
		var currentYear = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
		if(currentYear != ''){
			$('#sidebar a[title="'+currentYear+'"]').closest('li').addClass('active');
		}
	}
	
	
	if($('body.type-12').length || $('body.type-17').length ){
    $('#profile-gallery').cycle({
      speed: 2000,
      timeout: 5500,
      fx: 'fade',
      width: '100%',
      fit: 1,
      prev: '.arrows .prev',
      next: '.arrows .next'
    }); 
    
    $('nav ul ul').hide();
    
	}//if
	
	$('object.svg-object').hide(); 
	$('object.svg-object').each(function(index){
		$(this).parent().find('svg').attr("id",'svg'+index);
		var svgId = '#svg'+index;
		var svgImage = '..'+$(this).attr("data");
		Snap.load(svgImage, svgLoaded);
		function svgLoaded(data){
			Snap(svgId).append(data);
		}
	});
	
	$('.columnize').columnizeList({columnAmount:2}); 
	

	function alternateSections(){
		if($('body.type-1').length || $('body.type-9').length ||  $('body.type-13').length){
			$('section.alt-0').each(function(){
				$(this).find('.image').insertAfter($(this).find('.desc'));
			});
		}
		$('section.alt-0').fadeTo( "fast" , 1);
	}
	
	function unalternateSections(){
		if($('body.type-1').length || $('body.type-9').length || $('body.type-13').length){
			$('section.alt-0').each(function(){
				$(this).find('.image').insertBefore($(this).find('.desc')); 
			});
		}
		$('section.alt-0').css({opacity: 1});
	} 
	
	function desktopTeamColumns(){
		if($('body.type-3').length){
			if(window.location.hash  === '' && !$('body.page-1073').length){
				window.location.hash = 'Leadership';
			}
			$('#team-landing td[data-category*="Leadership"]').prependTo('#team-landing table');
			$('tr > td').unwrap();
			var $children = $('#team-landing td');
	    for(var i = 0, l = $children.length; i < l; i += 3) {
	      $children.slice(i, i+3).wrapAll('<tr></tr>');
	    }
	    $('#team-landing table').fadeTo( "fast" , 1); 
	    if(!$('body.page-1073').length){
		    $('#sidebar .team-navigation li a').each(function(){
					$(this).click(function(e){
						e.preventDefault();
						window.location.hash = $(this).attr('data-hash');
						$('#sidebar .team-navigation li').removeClass('active');
						$(this).parent().addClass('active');
						$('tr > td').unwrap();
						var category = $(this).attr('data-category');
						$('#team-landing td').hide();
						$('#team-landing td[data-category*="'+category+'"]').show(); 
						var $newchildren = $('#team-landing td:visible');
					    for(var i = 0, l = $newchildren.length; i < l; i += 3) {
					      $newchildren.slice(i, i+3).wrapAll('<tr></tr>');
				    } 
					});
				});
	    }else{
		    $('#sidebar .team-navigation li a').each(function(){
			    var originalHref = $(this).attr('href');
			    var itemHash = $(this).attr('data-hash');
			    var hashHref = originalHref+'#'+itemHash;
			    $(this).attr('href',hashHref);
		    });
	    }
	    
			var urlCategory = window.location.hash.substr(1);
			if(urlCategory != ''){
				console.log("activeCategory is"+urlCategory);
				$('#sidebar .team-navigation li').removeClass('active');
				var activeCategory = $('#sidebar .team-navigation li a[data-hash="'+urlCategory+'"]').attr('data-category');
				$('#sidebar .team-navigation li a[data-hash="'+urlCategory+'"]').parent().addClass('active');
				$('tr > td').unwrap();
				$('#team-landing td').hide();
				$('#team-landing td[data-category*="'+activeCategory+'"]').show(); 
				var $newchildren = $('#team-landing td:visible');
			    for(var i = 0, l = $newchildren.length; i < l; i += 3) {
			      $newchildren.slice(i, i+3).wrapAll('<tr></tr>');
		    } 
			}else if(urlCategory === ''){
				$('tr > td').unwrap();
				$('#team-landing td').show(); 
				var $newchildren = $('#team-landing td');
			    for(var i = 0, l = $newchildren.length; i < l; i += 3) {
			      $newchildren.slice(i, i+3).wrapAll('<tr></tr>');
		    } 
			}//else if 
		}//if
	}//desktop team columns
	
	function mobileTeamColumns(){
		if($('body.type-3').length){
			window.location.hash = '';
			$('#team-landing td[data-category*="Leadership"]').prependTo('#team-landing table');
			$('tr > td').unwrap();
			var $children = $('#team-landing td');
	    for(var i = 0, l = $children.length; i < l; i += 2) {
	      $children.slice(i, i+2).wrapAll('<tr></tr>');
	    }
	    $('#team-landing table').fadeTo( "fast" , 1); 
/*
	    if(!$('body.page-1073').length){
		    $('#sidebar .team-navigation li a').each(function(){
					$(this).click(function(e){
						e.preventDefault();
						window.location.hash = $(this).attr('data-hash');
						$('#sidebar .team-navigation li').removeClass('active');
						$(this).parent().addClass('active');
						$('tr > td').unwrap();
						var category = $(this).attr('data-category');
						$('#team-landing td').hide();
						$('#team-landing td[data-category*="'+category+'"]').show(); 
						var $newchildren = $('#team-landing td:visible');
					    for(var i = 0, l = $newchildren.length; i < l; i += 2) {
					      $newchildren.slice(i, i+2).wrapAll('<tr></tr>');
				    } 
					});
				}); 
	    }	    
*/
/*
			var urlCategory = window.location.hash.substr(1);
			if(urlCategory != ''){
				console.log("activeCategory is"+urlCategory);
				$('#sidebar .team-navigation li').removeClass('active');
				var activeCategory = $('#sidebar .team-navigation li a[data-hash="'+urlCategory+'"]').attr('data-category');
				$('#sidebar .team-navigation li a[data-hash="'+urlCategory+'"]').parent().addClass('active');
				$('tr > td').unwrap();
				$('#team-landing td').hide();
				$('#team-landing td[data-category*="'+activeCategory+'"]').show(); 
				var $newchildren = $('#team-landing td:visible');
			    for(var i = 0, l = $newchildren.length; i < l; i += 2) {
			      $newchildren.slice(i, i+2).wrapAll('<tr></tr>');
		    } 
			}else if(urlCategory === ''){
				$('tr > td').unwrap();
				$('#team-landing td').show(); 
				var $newchildren = $('#team-landing td');
			    for(var i = 0, l = $newchildren.length; i < l; i += 3) {
			      $newchildren.slice(i, i+3).wrapAll('<tr></tr>');
		    } 
			}//else if  
*/ 
		}//if
	}//mobile team columns
	
	function desktopProfileColumns(){
		if($('body.type-5').length || $('body.type-10').length){
			$('tr > td').unwrap();
			var $children = $('#project-profiles td');
	    for(var i = 0, l = $children.length; i < l; i += 3) {
	      $children.slice(i, i+3).wrapAll('<tr></tr>');
	    }
		}
	}
	
	function mobileProfileColumns(){
		if($('body.type-5').length || $('body.type-10').length){
			$('tr > td').unwrap();
			var $children = $('#project-profiles td');
	    for(var i = 0, l = $children.length; i < l; i += 2) {
	      $children.slice(i, i+2).wrapAll('<tr></tr>');
	    }
		}
	}
	
	function desktopSidebar(){
		if($('#sidebar.categories').length){
			$('li.title').addClass('active').nextAll().show();
		}
	}
	
	function mobileSidebar(){
		if($('#sidebar.categories').length){
			$('li.title').removeClass('active').nextAll().hide();
			$('li.title').click(function(){
				$(this).toggleClass('active');
				$(this).nextAll().toggle(); 
			});
		}
	}
	
	
  function slideDrawerEnable(){
    var range = MQ.new_context;
    var trigger = '.snap-trigger-';
    var content = '#content_container';
    var drawer = '.snap-drawer-';
    //var contentBorder = '#content_push';
    $('.snap-drawers').show();
    $(content).addClass('active');
    $(trigger+'left').show();
    $(trigger+'right').show();
    $('#content_container.active').unbind();
    $(trigger+'left').click(function(){
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });

      }else{
        $(content).addClass('open-left').stop().animate({marginLeft: '200px'}, function(){
           $('#content_container.active').click(function(){
            //alert("click");
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-left');
        $(drawer+'left').show();
      }
    });
    $(trigger+'right').click(function(){
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
          //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });
      }
      if($(content+'.open-right').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-right');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-right');
           $('#content_container.active').unbind();
        });
      }else{
        $(content).addClass('open-right').stop().animate({marginLeft: '-200px'}, function(){
           $(drawer+'left').hide();
           $('#content_container.active').click(function(){
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-right');
        $(drawer+'right').show();
      }
    });
  }//snapDrawerEnable

  function slideDrawerDisable(){
    var trigger = '.snap-trigger-';
    var content = '#content_container';
    //var contentBorder = '#content_push';
    $('.snap-drawers').hide();
    $(content).removeClass('active');
    $('#content_container.active').unbind();
  	$(trigger+'left').unbind().hide();
    $(trigger+'right').unbind().hide();
    if($(content+'.open-left').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-left');
        //$(contentBorder).removeClass('open-left');
      });
    }
    if($(content+'.open-right').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-right');
        //$(contentBorder).removeClass('open-right');
      });
    }
  }


}); //End Document Ready